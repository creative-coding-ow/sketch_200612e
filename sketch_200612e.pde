int lastX;
int lastY;
int lastDirection;
int lineLength;

void setup(){
  size(1000,1000);
  lastX = width/2;
  lastY = height/2;
  lastDirection = 0;
  lineLength = 100;
  print(lastX);
  grow();
}

void draw(){
  grow();
  saveFrame("images/image####.png");
}

void mousePressed(){
  grow();
}

void grow(){
  int newX = 0;
  int newY = 0;
  int seed = (int)random(0,1000);
  int upOrDown = (seed%2==0) ? 1 : -1;
  
  switch(seed%3){
  case 0:
    newX = lastX + lineLength * upOrDown;
    newY = lastY;
    break;
  case 1:
    newX = lastX;
    newY = lastY + lineLength * upOrDown;
    break;
  case 2:
    newX = lastX + lineLength * upOrDown;
    newY = lastY + lineLength * upOrDown;
    break;
  }
  
  line(lastX, lastY, newX, newY);
           
  lastX = newX % width;
  lastY = newY % height;
}
